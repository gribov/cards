<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManagerInterface;
use PetstoreIO\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use UserBundle\Entity\Token;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20200426164325 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        /** @var EntityManagerInterface $em */
        $em = $this->container->get('doctrine.orm.entity_manager');
        /** @var \UserBundle\Entity\Token $token */
        $token = $em->getRepository(Token::class)->findOneBy(['accessToken' => 'accesstoken123456789123456789012']);
        $token->setExpiresAt(new \DateTime('+120 month'));
        $em->persist($token);
        $em->flush();

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
    }
}
