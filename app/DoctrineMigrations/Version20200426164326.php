<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use UserBundle\Entity\Token;
use UserBundle\Entity\User;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20200426164326 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        /** @var EntityManagerInterface $em */
        $em = $this->container->get('doctrine.orm.entity_manager');
        $user = new User();
        $user->setEmail('admin@test.ru');
        $user->setUsername('admin');

        $user->setPasswordHash('$2y$12$H2scXyPDJ0/.xUEKwMG2Q.6Cctdp2z9dfeQRRadmRJ7bb.9rzoqhm'); // pass
        $user->setRoles([User::ROLE_ADMIN]);

        $token = new Token();
        $token->setUser($user);
        $token->setAccessToken('accesstoken123456789123456782020');
        $token->setRefreshToken('refresh1token1123456789123452020');
        $token->setExpiresAt(new \DateTime('+12 month'));

        $em->persist($user);
        $em->persist($token);
        $em->flush();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
    }
}
